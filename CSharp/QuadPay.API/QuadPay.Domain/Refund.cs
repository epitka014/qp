using System;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.ValueObjects;

namespace QuadPay.Domain
{
    internal class Refund
    {
        internal Guid Id { get; }

        internal string IdempotencyKey { get; }

        internal DateTime Date { get; }

        // since it seems like we can request refund for more than what is available for refund
        // this amount is actually requested refund amount, not what actually was refunded. Not sure if it 
        // is important in the domain 
        internal Money Amount { get; }

        internal Refund(IdempotencyKey idempotencyKey, Money amount)
        {
            Guard.NotNull(amount, nameof(amount));
            Guard.NotNull(idempotencyKey,nameof(idempotencyKey));

            if (amount == 0)
            {
                throw new ValidationException("Amount to refund must be greater than 0");
            }

            Id = Guid.NewGuid();
            Date = DateTime.Now;

            IdempotencyKey = idempotencyKey;
            Amount = amount;
        }
    }
}