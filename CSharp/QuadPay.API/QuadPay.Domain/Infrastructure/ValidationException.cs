﻿using System;

namespace QuadPay.Domain.Infrastructure
{
    public class ValidationException : Exception
    {
        public ValidationException(string message) : base(message)
        {

        }
    }
}
