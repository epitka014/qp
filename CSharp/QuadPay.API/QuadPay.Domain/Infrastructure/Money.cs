﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuadPay.Domain.Infrastructure
{
    public class Money
    {
        public Money(decimal amt)
        {
            EnsureValid(amt);
            _value = Math.Round(amt,2);
        }

        private decimal _value;
        public decimal Value => _value;

        public static implicit operator decimal(Money value)
        {
            return value._value;
        }

        public static implicit operator Money(decimal value)
        {
            return new Money(value);
        }

        private void EnsureValid( decimal amt)
        {
           if (amt<0) throw new ValidationException("Amount cannot be less than 0");
            
        }
    }
}
