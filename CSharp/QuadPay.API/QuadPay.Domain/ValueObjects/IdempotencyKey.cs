﻿using System;
using System.Collections.Generic;
using System.Text;
using QuadPay.Domain.Infrastructure;

namespace QuadPay.Domain.ValueObjects
{
    internal class IdempotencyKey
    {
        internal IdempotencyKey(string value)
        {
            EnsureValid(value);

            _value = value;
        }

        private readonly string _value;
        internal string Value => _value;

        private void EnsureValid(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ValidationException("IdempotencyKey cannot be empty string, white space or null");
            }
        }

        public static implicit operator string(IdempotencyKey paymentReference)
        {
            return paymentReference._value;
        }

        public static implicit operator IdempotencyKey(string value)
        {
            return new IdempotencyKey(value);
        }
    }
}
