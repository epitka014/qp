﻿using System;
using System.Collections.Generic;
using System.Text;
using QuadPay.Domain.Infrastructure;

namespace QuadPay.Domain.ValueObjects
{
    internal class InstallmentPercent
    {
        private readonly decimal _value;
        public decimal Value => _value;

        internal InstallmentPercent(decimal value)
        {
            EnsureValid(value);

            _value = Math.Round(value, 2);
        }

        private void EnsureValid(decimal value)
        {
            if (value <= 0 || value > 100)
            {
                throw new ValidationException("Percent has to be greater than 0 and less than 100");
            }

        }

        public static implicit operator decimal(InstallmentPercent value)
        {
            return value._value;
        }

        public static implicit operator InstallmentPercent(decimal value)
        {
            return new InstallmentPercent(value);
        }
    }
}
