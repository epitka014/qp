﻿using System;
using System.Collections.Generic;
using System.Text;
using QuadPay.Domain.Infrastructure;

namespace QuadPay.Domain.ValueObjects
{
    internal class InstallmentInterval
    {
        private readonly int _value;

        internal int Value => _value;
        
        public InstallmentInterval(int value=14)
        {
            EnsureValid(value);

            _value = value;
        }

        public static implicit operator int (InstallmentInterval value)
        {
            return value._value;
        }

        public static implicit operator InstallmentInterval(int value)
        {
            return new InstallmentInterval(value);
        }

        private void EnsureValid(int value)
        {
            // Assume that interval cannot be less than 14 and more than 21 days

            if (value < 14 || value > 21)
            {
                throw new ValidationException("Installment interval cannot be less than 14 day and more than 21 days");
            }
        }
    }
}

