﻿using System;
using System.Collections.Generic;
using System.Text;
using QuadPay.Domain.Infrastructure;

namespace QuadPay.Domain.ValueObjects
{
    internal class PaymentReference
    {
        internal PaymentReference(string value)
        {
            EnsureValid(value);

            _value = value;
        }

        private readonly string _value;
        internal string Value => _value;

        private void EnsureValid(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ValidationException("Payment reference cannot be empty string, white space or null");
            }
        }

        public static implicit operator string(PaymentReference paymentReference)
        {
            return paymentReference._value;
        }

        public static implicit operator PaymentReference(string value)
        {
            return new PaymentReference(value);
        }
    }
}
