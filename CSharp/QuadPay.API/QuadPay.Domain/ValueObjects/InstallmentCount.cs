﻿using System;
using System.Collections.Generic;
using System.Text;
using QuadPay.Domain.Infrastructure;

namespace QuadPay.Domain.ValueObjects
{
    internal class InstallmentCount
    {
        private readonly int _value;

        internal int Value => _value;

        public InstallmentCount(int value=4)
        {
            EnsureValid(value);

            _value = value;
        }

        public static implicit operator int (InstallmentCount value)
        {
            return value._value;
        }

        public static implicit operator InstallmentCount(int value)
        {
            return new InstallmentCount(value);
        }

        private void EnsureValid(int value)
        {
            // Assume that number of payments cannot be less than 4
            // and more than 8

            if (value < 4 || value > 8)
            {
                throw new ValidationException("Number of installments cannot be less than 4 and more than 8");
            }
        }

       
    }
}
