﻿using System;
using System.Collections.Generic;
using System.Text;
using QuadPay.Domain.Infrastructure;

namespace QuadPay.Domain.Commands
{
    internal class ApplyRefund
    {
        public class Command
        {
            private readonly Guid _paymentPlanId;
            private readonly Money _amountToRefund;

            public Guid PaymentPlanId => _paymentPlanId;
            public Money AmountToRefund => _amountToRefund;

            public Command(Guid paymentPlanId, Money amountToRefund)
            {
                _paymentPlanId = paymentPlanId;
                _amountToRefund = amountToRefund;
            }
        }

        public class Handler
        {
            public Handler(Command cmd)
            {
            }
        }
    }
}
