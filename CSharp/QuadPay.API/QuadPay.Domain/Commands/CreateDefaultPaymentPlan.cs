﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.Services;
using QuadPay.Domain.ValueObjects;

namespace QuadPay.Domain.Commands
{
    internal class CreateDefaultPaymentPlan
    {
        internal class Command
        {
            private readonly Money _amountToSplit;
           
            internal Money AmountToSplit => _amountToSplit;

            internal Command(Money amountToSplit)
            {
                Guard.NotNull(amountToSplit, nameof(amountToSplit));
               
                _amountToSplit = amountToSplit;
              
            }
        }

        internal class Handler
        {
            private readonly IInstallmentFactory _installmentFactory;

            public Handler(IInstallmentFactory installmentFactory)
            {
                _installmentFactory = installmentFactory;
            }

            public PaymentPlan Handle(Command command)
            {
                var installments = _installmentFactory.GetInstallments(DateTime.Now, command.AmountToSplit,
                                            installmentCount:4, installmentInterval:14, firstInstallmentPercent: 25);

                var toReturn = new PaymentPlan(installments);
              
                //perist and raise
                //event PaymentPlanCreated
               
                return toReturn;
            }
        }
    }
}

