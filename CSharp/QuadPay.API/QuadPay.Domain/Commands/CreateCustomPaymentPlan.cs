﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.Services;
using QuadPay.Domain.ValueObjects;

namespace QuadPay.Domain.Commands
{
    internal class CreateCustomPaymentPlan
    {
        internal class Command
        {
            private readonly Money _amountToSplit;
            private readonly InstallmentCount _installmentCount;
            private readonly InstallmentInterval _installmentInterval;

            internal Money AmountToSplit => _amountToSplit;
            internal InstallmentCount InstallmentCount => _installmentCount;
            internal InstallmentInterval InstallmentInterval => _installmentInterval;

            internal Command(Money amountToSplit, InstallmentCount installmentCount,
                InstallmentInterval installmentInterval)
            {
                Guard.NotNull(amountToSplit, nameof(amountToSplit));
                Guard.NotNull(installmentCount, nameof(installmentCount));
                Guard.NotNull(installmentInterval, nameof(installmentInterval));

                _amountToSplit = amountToSplit;
                _installmentCount = installmentCount;
                _installmentInterval = installmentInterval;
            }
        }

        internal class Handler
        {
            private readonly IInstallmentFactory _installmentFactory;

            public Handler(IInstallmentFactory installmentFactory)
            {
                _installmentFactory = installmentFactory;
            }

            public PaymentPlan Handle(Command command)
            {
                var installments = _installmentFactory.GetEqualInstallments(DateTime.Now, command.AmountToSplit,
                                            command.InstallmentCount, command.InstallmentInterval);

                var toReturn = new PaymentPlan(installments);
              
                //perist and raise
                //event PaymentPlanCreated
               
                return toReturn;
            }
        }
    }
}

