﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.ValueObjects;

namespace QuadPay.Domain.Services
{
    internal interface IInstallmentFactory
    {
        IEnumerable<Installment> GetEqualInstallments(DateTime startDate, Money amountToSplit,
            InstallmentCount installmentCount,
            InstallmentInterval installmentInterval);

        IEnumerable<Installment> GetInstallments(DateTime startDate, Money amountToSplit,
            InstallmentCount installmentCount,
            InstallmentInterval installmentInterval,
            InstallmentPercent firstInstallmentPercent);
    }

    internal class InstallmentFactory : IInstallmentFactory
    {
        private readonly IPaymentSpliter _paymentSplitter;
        private readonly IPaymentDueDateCalculator _dueDateCalculator;

        public InstallmentFactory(IPaymentSpliter paymentSpliter, IPaymentDueDateCalculator dueDateCalculator)
        {
            _paymentSplitter = paymentSpliter;
            _dueDateCalculator = dueDateCalculator;
        }

        public IEnumerable<Installment> GetEqualInstallments(DateTime startDate, Money amountToSplit,
            InstallmentCount installmentCount,
            InstallmentInterval installmentInterval)
        {
            Guard.NotNull(amountToSplit, nameof(amountToSplit));
            Guard.NotNull(installmentCount, nameof(installmentCount));
            Guard.NotNull(installmentInterval, nameof(installmentInterval));

            var toReturn = new List<Installment>();

            var amounts = _paymentSplitter.Execute(amountToSplit, installmentCount).ToArray();
            var dueDates = _dueDateCalculator.Execute(startDate, installmentInterval, installmentCount).ToArray();

            for (var i = 0; i < installmentCount; i++)
            {
                toReturn.Add(new Installment(amounts[i], dueDates[i]));
            }

            return toReturn;
        }

        public IEnumerable<Installment> GetInstallments(DateTime startDate, Money amountToSplit,
                                        InstallmentCount installmentCount, InstallmentInterval installmentInterval,
                                        InstallmentPercent firstInstallmentPercent)
        {
            Guard.NotNull(amountToSplit, nameof(amountToSplit));
            Guard.NotNull(installmentCount, nameof(installmentCount));
            Guard.NotNull(installmentInterval, nameof(installmentInterval));

            var toReturn = new List<Installment>();

            var firstAmount = Math.Round(amountToSplit * firstInstallmentPercent);
            var firstDueDate = startDate;

            var firstInstallment = new Installment(firstAmount, firstDueDate);

            toReturn.Add(firstInstallment);

            amountToSplit -= firstAmount;

            //if we have to account for holidays, work days etc. we would 
            //have to call service here to adjust calculated date
            //same service coud be injected to InstallmentFactory
            startDate = startDate.AddDays(installmentInterval);

            var amounts = _paymentSplitter.Execute(amountToSplit, installmentCount).ToArray();
            var dueDates = _dueDateCalculator.Execute(startDate, installmentInterval, installmentCount).ToArray();

            for (var i = 0; i < installmentCount; i++)
            {
                toReturn.Add(new Installment(amounts[i], dueDates[i]));
            }

            return toReturn;
        }
    }
}
