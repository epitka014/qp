﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuadPay.Domain.Services
{
    public interface IDueDateAdjuster
    {
        DateTime Adjust(DateTime dueDate);
    }

    //Should adjust calculated due date by looking if the date is weekend, holiday etc. This info
    //could be culture specific.
    //It is just a place holder for now.
    public class DueDateAdjuster:IDueDateAdjuster
    {
        public DateTime Adjust(DateTime dueDate)
        {
            //check if holiday, weekend etc.
            return dueDate;
        }
    }
}
