﻿using System;
using System.Collections.Generic;
using QuadPay.Domain.ValueObjects;

namespace QuadPay.Domain.Services
{
    internal interface IPaymentDueDateCalculator
    {
        IEnumerable<DateTime> Execute(DateTime startDate, InstallmentInterval interval,
            InstallmentCount numberOfInstallments, bool adjustStartDate=false);
    }

    //NOTE: NodaTime would maybe be better choice to work with dates

    internal class PaymentDueDateCalculator : IPaymentDueDateCalculator
    {
        private readonly IDueDateAdjuster _dueDateAdjuster;

        public PaymentDueDateCalculator(IDueDateAdjuster dueDateAdjuster)
        {
            _dueDateAdjuster = dueDateAdjuster;
        }

        public IEnumerable<DateTime> Execute(DateTime startDate, 
                        InstallmentInterval interval, InstallmentCount numberOfInstallments,
                        bool adjustStartDate = false)
        {
            var toReturn = new List<DateTime>(numberOfInstallments);

            var dueDate = startDate;

            toReturn.Add(adjustStartDate ? _dueDateAdjuster.Adjust(startDate) : startDate);
            
            for (int i = 1; i < numberOfInstallments; i++)
            {
                dueDate = dueDate.AddDays(interval);
            
                //not sure if dueDate should fall only on working days, holidays etc.
                //if it should be moved to next working day, etc.
                toReturn.Add(_dueDateAdjuster.Adjust(dueDate));
            }

            return toReturn;
        }
    }
}