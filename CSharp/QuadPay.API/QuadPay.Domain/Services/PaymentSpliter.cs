﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.ValueObjects;

namespace QuadPay.Domain.Services
{


    internal interface IPaymentSpliter
    {
        IEnumerable<Money> Execute(Money amountToSplit, int numberOfPayments, bool applyRoundindDifferenceToFirst=true);
    }

    internal class PaymentSpliter : IPaymentSpliter
    {
        /// <summary>
        /// If applyRoundingDifferenceToFirst is set to false, rounding difference will be applied to last.
        /// </summary>
        /// <param name="amountToSplit"></param>
        /// <param name="numberOfPayments"></param>
        /// <param name="applyRoundindDifferenceToFirst"></param>
        /// <returns></returns>
        public IEnumerable<Money> Execute(Money amountToSplit, int numberOfPayments, bool applyRoundindDifferenceToFirst=true)
        {
            var toReturn = new List<Money>();

            var installmentAmount = Math.Round(amountToSplit/ numberOfPayments,2);

            //not sure if rounding difference to be applied to first or last payment
            var roundingDifference =  amountToSplit - (installmentAmount * numberOfPayments);

            var temp = installmentAmount + roundingDifference;

            for (int i = 0; i < numberOfPayments; i++)
            {
               toReturn.Add(installmentAmount); 
            }

            if (applyRoundindDifferenceToFirst)
            {
                toReturn[0] = temp;
            }
            else
            {
                toReturn[numberOfPayments - 1] = temp;
            }

            return toReturn;
        }

     }
}
