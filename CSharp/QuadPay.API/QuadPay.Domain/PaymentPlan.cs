﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.ValueObjects;

namespace QuadPay.Domain
{
    internal class PaymentPlan
    {
        internal Guid Id { get; }

        private List<Installment> _installments;
        internal IEnumerable<Installment> Installments => _installments;

        private List<Refund> _refunds;
        internal IEnumerable<Refund> Refunds => _refunds;

        /// <summary>
        /// Date when payment plan was created. Currently first payment is due immediatelly.
        /// </summary>
        internal DateTime OriginationDate { get; }

        private PaymentPlan()
        {
            _installments = new List<Installment>();
            _refunds = new List<Refund>();
        }

        /// <summary>
        /// Use InstallmentFactory to create installments
        /// </summary>
        /// <param name="installments"></param>
        internal PaymentPlan(IEnumerable<Installment> installments) : this()
        {
            //there is no check for number of payments here
            //this allows us to create any kind of payment plan
            //since  payment plan is created in command handler we can
            //enforce this default rule there.
            //Also, we could enforce rule here that the first payment must
            //be 25% and due on today, or we can do this in command handler
            //which gives us more flexibility in setting up payment plans.
            Guard.NotNull(installments, nameof(installments));

            if (installments.Any() == false)
            {
                throw new ValidationException("PaymentPlan must have at least one payment");
            }

            Id = Guid.NewGuid();

            OriginationDate = DateTime.Now;

            var temp = installments.OrderBy(x => x.DueDate).ToList();

            if (temp.First().DueDate.ToShortDateString() != OriginationDate.ToShortDateString())
            {
                //First payment is due immediately. See OriginationDateComment

                throw new ValidationException("First payment is due immediately");
            }

            _installments = temp;

        }

        // Installments are paid in order by Date
        internal Installment NextInstallment() => _installments.FirstOrDefault(x => x.IsPaid() == false);

        internal Installment FirstInstallment() => _installments.First();

        internal IEnumerable<Installment> PaidInstallments() => _installments.Where(x => x.IsPaid());

        //not sure if these are only that are due but not paid, or all unpaid installments
        //I'll assume it is the first one
        internal IEnumerable<Installment> PendingInstallments() => _installments.Where(x => x.IsPending());

        /// <summary>
        /// All installments not paid, regardless of due date
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<Installment> OutstandingInstallments() => _installments.Where(x => x.IsPaid() == false);

        internal IEnumerable<Installment> DefaultedInstallments() => _installments.Where(x => x.IsDefaulted());

        public decimal OustandingBalance()
        {
            return OutstandingInstallments().Sum(x => x.Amount);
        }

        //public decimal AmountPastDue(DateTime currentDate)
        public decimal AmountPastDueOn(DateTime date)

        {
            return _installments.Where(x => x.IsPaid() == false && x.DueDate.Date <= date.Date)
                                .Sum(x => x.Amount);
        }

        internal decimal MaximumRefundAvailable()
        {
            return PaidInstallments().Sum(x => x.AvailableForRefund());
        }

        // We only accept payments matching the Installment Amount.
        internal void MakePayment(Money amount, Guid installmentId)
        {
            if (OutstandingInstallments().Any() == false)
            {
                throw new ValidationException("There are no unpaid installments.");
            }

            var installment = Installments.FirstOrDefault(x => x.Id == installmentId);
            if (installment == null)
            {
                throw new ValidationException($"Installment {installmentId} is not associated with this payment plan.");
            }

            if (installment.IsPaid())
            {
                throw new ValidationException($"Installment {installmentId} has already been paid.");
            }

            //TODO: need to implement equality comparisson based on the value on Money and other value objects
            if (installment.Amount.Value != amount.Value)
            {
                throw new ValidationException($"Payment amount {amount.Value} is not equal to installment amount {installment.Amount.Value}. Installment {installment.Id}");
            }
            
            //where does the payment reference come from somewhere???
            installment.SetPaid(Guid.NewGuid().ToString());
            
        }

        // Returns: Amount to refund via PaymentProvider
        //NOTE: this operation queries and changes state. CQS states that question should not change the answer.

        internal void ApplyRefund(Refund refund, out Money actuallyRefunded)
        {
            Guard.NotNull(refund, nameof(refund));

            Money amountToRefund = 0;
            //
            
            if (_refunds.Any(x => x.IdempotencyKey == refund.IdempotencyKey))
            {
                //not sure if we would just ignore it or raise exception
                throw new ValidationException("Refund already processed");
            }

            //NOTE: based on one of the test cases seems like we will refund as much as we can.
            if (refund.Amount > MaximumRefundAvailable())
            {
                amountToRefund = MaximumRefundAvailable();
            }
            else
            {
                amountToRefund = refund.Amount;
            }
            
            //start issuing refunds from the back of the paid installments
            var installments = this.PaidInstallments()
                                        .Where(x => x.AvailableForRefund() > 0).Reverse();

            decimal refundedAmount = 0;
            foreach (var installment in installments)
            {
                if (amountToRefund == 0) break;

                var availableForRefund = installment.AvailableForRefund();
                
                if (amountToRefund >= availableForRefund)
                {
                    amountToRefund -= availableForRefund;

                    installment.Refund(availableForRefund);

                    refundedAmount += availableForRefund;
                }
                else
                {
                    installment.Refund(amountToRefund);

                    refundedAmount += amountToRefund;

                    amountToRefund = 0;
                }
            }

            actuallyRefunded = refundedAmount;

            _refunds.Add(refund);
        }
    }
}