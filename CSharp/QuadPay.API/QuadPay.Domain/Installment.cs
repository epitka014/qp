using System;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.ValueObjects;

namespace QuadPay.Domain
{
    internal class Installment
    {
        internal Guid Id { get; }

        internal DateTime DueDate { get; }

        internal Money Amount { get; }

        internal Money RefundedAmount { get; private set; }
        
        internal InstallmentStatus Status { get; private set; }

        internal string PaymentReference { get; private set; }

        // Date this Installment was marked 'Paid'
        internal DateTime SettlementDate { get; private set; }

        internal Installment(Money amount, DateTime dueDate)
        {
            Guard.NotNull(amount, nameof(amount));
            
            Id = Guid.NewGuid();

            Amount = amount;

            //??? should date be converted so that time portion is midnight 11:59:99 ??
            DueDate = dueDate;
            
            Status = InstallmentStatus.Pending;

            RefundedAmount = 0;
        }

        internal bool IsPaid() => Status == InstallmentStatus.Paid;

        internal bool IsDefaulted() => Status == InstallmentStatus.Defaulted;

        internal bool IsPending() => Status == InstallmentStatus.Pending;

        //instead of creating operation for each status we can invert question
        internal bool IsInStatus(InstallmentStatus status)
        {
            return Status == status;
        }

        internal void SetPaid(PaymentReference paymentReference)
        {
            Guard.NotNull(paymentReference, nameof(paymentReference));

            if (IsPaid())
            {
                throw new ValidationException("Installment has already been paid.");
            }

            PaymentReference = paymentReference;
            
            //it may be better to set these 2 in the setter for PaymentReference
            SettlementDate = DateTime.Now;

            Status = InstallmentStatus.Paid;
        }

        internal decimal AvailableForRefund()
        {
            if (IsPaid())
            {
                return Amount - RefundedAmount;
            }

            return 0;
        }

        internal void Refund(Money amount)
        {
            Guard.NotNull(amount, nameof(amount));

            if (IsPaid() == false)
            {
                throw new ValidationException("Cannot refund installment that has not been paid.");
            }

            if (amount > AvailableForRefund())
            {
                throw new ValidationException("Cannot refund more that available for refund.");
            }

            RefundedAmount += amount;
        }
    }

    internal enum InstallmentStatus
    {
        Pending = 0, // Not yet paid
        Paid = 1, // Can be either paid with a charge, or covered by a refund
        Defaulted = 2,// Charge failed
    }
}