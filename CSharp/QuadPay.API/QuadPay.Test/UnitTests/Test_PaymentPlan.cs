﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using FakeItEasy;
using QuadPay.Domain;
using QuadPay.Domain.Infrastructure;
using Shouldly;
using Xunit;

namespace QuadPay.Test.UnitTests
{
    public class Test_PaymentPlan
    {

        public class Ctor
        {
            [Fact]
            public void When_null_installments_is_null_throws()
            {
                Assert.Throws<ArgumentNullException>(() => new PaymentPlan(null));
            }

            [Fact]
            public void When_installments_is_empty_throws()
            {
                Assert.Throws<ValidationException>(() => new PaymentPlan(new List<Installment>()));
            }

            [Fact]
            public void When_installments_has_items_constructs_in_valid_state()
            {
                
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var sut = new PaymentPlan(installments);
                
                sut.Id.ShouldNotBeNull("Id");

                sut.OriginationDate.Date.ShouldBe(DateTime.Now.Date.Date,"OriginationDate");

                sut.Installments.Count().ShouldBe(installments.Count, "Installments");
                sut.DefaultedInstallments().Count().ShouldBe(0,"DefaultedInstallments");
                sut.Refunds.Count().ShouldBe(0,"Refunds");
                sut.FirstInstallment().ShouldBe(installments.First());
                sut.NextInstallment().ShouldBe(installments.First());
                sut.MaximumRefundAvailable().ShouldBe(0);
                sut.OutstandingInstallments().Count().ShouldBe(installments.Count,"OutstandingInstallments");
                sut.PaidInstallments().Count().ShouldBe(0,"PaidInstallments");
                sut.PendingInstallments().Count().ShouldBe(installments.Count,"PendingInstallments");
                sut.OustandingBalance().ShouldBe(installments.Sum(x=>x.Amount));
            }
        }

        public class MakePayment
        {
            [Fact]
            public void Can_make_payment()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                plan.MakePayment(100, installments.First().Id);

                plan.NextInstallment().ShouldBe(installments[1]);
                plan.MaximumRefundAvailable().ShouldBe(100);
                plan.OutstandingInstallments().Count().ShouldBe(installments.Count-1, "OutstandingInstallments");
                plan.PaidInstallments().Count().ShouldBe(1, "PaidInstallments");
                plan.PendingInstallments().Count().ShouldBe(installments.Count - 1, "PendingInstallments");
                plan.OustandingBalance().ShouldBe(installments[1].Amount.Value);

                plan.Installments.First().Status.ShouldBe(InstallmentStatus.Paid);

                //following should not have changed
                plan.Installments.Count().ShouldBe(installments.Count, "Installments");
                plan.DefaultedInstallments().Count().ShouldBe(0, "DefaultedInstallments");
                plan.Refunds.Count().ShouldBe(0, "Refunds");
                plan.FirstInstallment().ShouldBe(installments.First());

                
            }

            [Fact]
            public void When_no_outstanding_installments_throws()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                plan.MakePayment(100, installments[0].Id);
                plan.MakePayment(100,installments[1].Id);

                var ex = Assert.Throws<ValidationException>(() => plan.MakePayment(100, installments[1].Id));

                ex.Message.ShouldBe("There are no unpaid installments.");
            }

            [Fact]
            public void When_installment_is_not_on_payment_plan_throws()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                var installmentId = Guid.NewGuid();
                var ex = Assert.Throws<ValidationException>(() => plan.MakePayment(100, installmentId));

                ex.Message.ShouldBe($"Installment {installmentId} is not associated with this payment plan.");
            }

            [Fact]
            public void When_installment_has_been_already_paid_throws()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                var installmentId = installments.First().Id;

                plan.MakePayment(100,installmentId);

                var ex = Assert.Throws<ValidationException>(() => plan.MakePayment(100, installmentId));

                ex.Message.ShouldBe($"Installment {installmentId} has already been paid.");
            }

            [Fact]
            public void When_payment_amount_does_not_match_throws()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                var installment = installments.First();
                var installmentId = installment.Id;

                var amount = 10;

               var ex = Assert.Throws<ValidationException>(() => plan.MakePayment(amount, installmentId));

                ex.Message.ShouldBe($"Payment amount {amount} is not equal to installment amount {installment.Amount.Value}. Installment {installment.Id}");
            }
        }


        public class ApplyRefund
        {
            [Fact]
            public void When_Refund_is_null_throws()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                Money amt;
                Assert.Throws<ArgumentNullException>(()=>plan.ApplyRefund(null,out amt ));
            }

            //example using Mocking framework, but would require virtual members, protected parameterless constructors

            //[Fact]
            //public void When_refund_with_same_IdempotencyKey_exists_throws()
            //{
            //    var key = Guid.NewGuid().ToString();

            //    var fakePlan = A.Fake<PaymentPlan>();

            //    var fakeRefund = A.Fake<Refund>();

            //    A.CallTo(() => fakeRefund.IdempotencyKey).Returns(key);

            //    Money moneyToRefund;
            //    A.CallTo(() => fakePlan.ApplyRefund(fakeRefund, out moneyToRefund)).CallsBaseMethod();


            //   var ex= Assert.Throws<ValidationException>(() => fakePlan.ApplyRefund(fakeRefund, out moneyToRefund));

            //    ex.Message.ShouldBe("Refund already processed");

            //}

            [Fact]
            public void When_amount_of_paid_installments_matches_refquested_refund_should_refund_all_money()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                plan.MakePayment(100,installments[0].Id);
                plan.MakePayment(100,installments[1].Id);

                var key = Guid.NewGuid().ToString();

                var refund = new Refund(key,installments.Sum(x=>x.Amount));
                Money actualRefund;

                //act
                plan.ApplyRefund(refund,out actualRefund);

                //assert
                actualRefund.Value.ShouldBe(installments.Sum(x => x.Amount));

                plan.Refunds.Count().ShouldBe(1);
                plan.Refunds.First().ShouldBe(refund);

                plan.MaximumRefundAvailable().ShouldBe(0);
                plan.PaidInstallments().Count().ShouldBe(2);

                plan.Installments.ElementAt(0).ShouldBe(installments[0]);
                plan.Installments.ElementAt(1).ShouldBe(installments[1]);

                installments[0].RefundedAmount.Value.ShouldBe(100);
                installments[0].AvailableForRefund().ShouldBe(0);
                
                installments[1].RefundedAmount.Value.ShouldBe(100);
                installments[1].AvailableForRefund().ShouldBe(0);

            }

            [Fact]
            public void When_amount_of_requested_refund_is_equal_to_last_installment_paid_should_refund_that_installment()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                plan.MakePayment(100, installments[0].Id);
                plan.MakePayment(100, installments[1].Id);

                var key = Guid.NewGuid().ToString();


                var refund = new Refund(key, installments.Last().Amount);
                Money actualRefund;

                //act
                plan.ApplyRefund(refund, out actualRefund);

                //assert
                actualRefund.Value.ShouldBe(installments.Last().Amount.Value,"actuallRefund");

                plan.MaximumRefundAvailable().ShouldBe(installments.Sum(x=>x.Amount)-installments.Last().Amount,"maxRefundAvailable");

                installments[0].RefundedAmount.Value.ShouldBe(0,"refundedAmount_0");
                installments[0].AvailableForRefund().ShouldBe(100, "availableForRefund_0");

                installments[1].RefundedAmount.Value.ShouldBe(installments.Last().Amount.Value,"refundedAmount_1");
                installments[1].AvailableForRefund().ShouldBe(0,"availableForRefund_1");

            }

            [Fact]
            public void When_amount_of_requested_refund_is_less_then_amount_of_last_installment_paid_should_refund()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                plan.MakePayment(100, installments[0].Id);
                plan.MakePayment(100, installments[1].Id);

                var key = Guid.NewGuid().ToString();
                
                var refund = new Refund(key, 50);
                Money actualRefund;

                //act
                plan.ApplyRefund(refund, out actualRefund);

                //assert
                actualRefund.Value.ShouldBe(refund.Amount.Value, "actuallRefund");

                plan.MaximumRefundAvailable().ShouldBe(installments.Sum(x => x.Amount) - refund.Amount, "maxRefundAvailable");

                installments[0].RefundedAmount.Value.ShouldBe(0, "refundedAmount_0");
                installments[0].AvailableForRefund().ShouldBe(100, "availableForRefund_0");

                installments[1].RefundedAmount.Value.ShouldBe(refund.Amount.Value, "refundedAmount_1");
                installments[1].AvailableForRefund().ShouldBe(installments[1].Amount-refund.Amount, "availableForRefund_1");
            }

            [Fact]
            public void When_amount_of_requested_refund_is_more_that_sum_of_all_installments_paid_should_refund_the_sum_amount()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                plan.MakePayment(100, installments[0].Id);
                plan.MakePayment(100, installments[1].Id);

                var key = Guid.NewGuid().ToString();

                var refund = new Refund(key, installments.Sum(x => x.Amount)+100);
                Money actualRefund;

                //act
                plan.ApplyRefund(refund, out actualRefund);

                //assert
                actualRefund.Value.ShouldBe(installments.Sum(x => x.Amount));

              
                plan.MaximumRefundAvailable().ShouldBe(0);
              

                installments[0].RefundedAmount.Value.ShouldBe(100);
                installments[0].AvailableForRefund().ShouldBe(0);

                installments[1].RefundedAmount.Value.ShouldBe(100);
                installments[1].AvailableForRefund().ShouldBe(0);
            }

            [Fact]
            public void When_refund_with_same_IdempotencyKey_exists_throws()
            {
                var installments = new List<Installment>()
                {
                    new Installment(100, DateTime.Now),
                    new Installment(100, DateTime.Now.AddDays(14))
                };

                var plan = new PaymentPlan(installments);

                plan.MakePayment(100, installments[0].Id);
                plan.MakePayment(100, installments[1].Id);

                var key = Guid.NewGuid().ToString();

                var refund = new Refund(key, installments.Sum(x => x.Amount) + 100);
                Money actualRefund;

                
                plan.ApplyRefund(refund, out actualRefund);

               var ex = Assert.Throws<ValidationException>(() => plan.ApplyRefund(refund, out actualRefund));

                ex.Message.ShouldBe("Refund already processed");
            }
        }
    }
}
