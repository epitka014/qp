﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuadPay.Domain.Services;
using Shouldly;
using Xunit;

namespace QuadPay.Test.UnitTests.Services
{
    public class Test_InstallmentFactory
    {
        [Fact]
        public void Can_Get_Installments()
        {
            //Note: this should be newed up by DI framework
            var dueDateAdjuster = new DueDateAdjuster();
            var dueDateCalculator = new PaymentDueDateCalculator(dueDateAdjuster);
            var paymentSplitter = new PaymentSpliter();
            var dueDate = DateTime.Now;
            var installmentCount = 4;
            var installmentInterval = 14;
            var amountToSplit = 100;

            var factory = new InstallmentFactory(paymentSplitter, dueDateCalculator);

            var installments = factory.GetEqualInstallments(dueDate, amountToSplit, installmentCount, installmentInterval)
                                        .ToList();

            var expectedDueDates = dueDateCalculator.Execute(dueDate, installmentInterval, installmentCount)
                                                .ToList();
            var expectedInstallmentAmounts = paymentSplitter.Execute(amountToSplit, installmentCount)
                                                .ToList();

            installments.Count().ShouldBe(installmentCount,"installmentCount");

            for (int i = 0; i < installmentCount; i++)
            {
                installments[i].Amount.Value.ShouldBe(expectedInstallmentAmounts[i].Value,"installmentAmount");
                installments[i].DueDate.Date.ShouldBe(expectedDueDates[i].Date,"dueDate");
            }
        }
    }
}
