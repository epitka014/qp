﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuadPay.Domain.Services;
using Shouldly;
using Xunit;
using Xunit.Abstractions;

namespace QuadPay.Test.UnitTests.Services
{
    public class Test_PaymentSplitter
    {
        private readonly ITestOutputHelper _output;

        public Test_PaymentSplitter(ITestOutputHelper output)
        {
            {
                _output = output;
            }
        }
        //NOTE: using FSCheck would be much better option, but don't have time to set it up 
        [Theory]
        [InlineData(123.33,4)]
        [InlineData(123.34, 5)]
        [InlineData(113.03, 7)]
        [InlineData(101, 4)]
        [InlineData(2123.07, 7)]
        [InlineData(223.33, 6)]
        [InlineData(123.33, 5)]
        [InlineData(123.33, 9)]
        public void Sum_of_parts_should_match_amount_to_split(decimal amountToSplit, int numberOfPayments)
        {
            var obj = new PaymentSpliter();

            var result = obj.Execute(amountToSplit, numberOfPayments, false);


            result.Sum(x=>x).ShouldBe(amountToSplit,"amountToSplit");

        }

        //NOTE: using FSCheck would be much better option, but don't have time to set it up 
        [Theory]
        [InlineData(100, 4)]
        [InlineData(100, 5)]
        [InlineData(100, 7)]
        public void Should_split_amount_in_numberOfPayments(decimal amountToSplit, int numberOfPayments)
        {
            var obj = new PaymentSpliter();

            var result = obj.Execute(amountToSplit, numberOfPayments, false);

            result.Count().ShouldBe(numberOfPayments);

        }

        [Theory]
        [InlineData(123.33, 4)]
        public void Should_apply_rounding_difference_to_last_element(decimal amountToSplit, int numberOfPayments)
        {
            var obj = new PaymentSpliter();

            var result = obj.Execute(amountToSplit, numberOfPayments, false).ToList();

            _output.WriteLine(string.Join("," ,result.Select(x=>x.Value)));

            var items = result.Take(result.Count() - 1).ToList();

            Assert.All(items, item => item.Value.ShouldBe(items[0].Value));

            result.Last().Value.ShouldBeGreaterThan(items[0].Value);
        }

        [Theory]
        [InlineData(123.33, 4)]
        public void Should_apply_rounding_difference_to_first_element_by_default(decimal amountToSplit, int numberOfPayments)
        {
            var obj = new PaymentSpliter();

            var result = obj.Execute(amountToSplit, numberOfPayments).ToList();

            _output.WriteLine(string.Join(",", result.Select(x => x.Value)));

            var items = result.Skip(1).Take(result.Count() - 1).ToList();

            Assert.All(items, item => item.Value.ShouldBe(items[0].Value));

            result.First().Value.ShouldBeGreaterThan(items[0].Value);
        }
    }
}
