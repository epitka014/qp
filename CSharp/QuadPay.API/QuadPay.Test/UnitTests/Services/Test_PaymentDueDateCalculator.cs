﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuadPay.Domain.Services;
using Shouldly;
using Xunit;

namespace QuadPay.Test.UnitTests.Services
{
    public class Test_PaymentDueDateCalculator
    {
        [Theory]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(6)]
        public void Due_dates_count_should_match(int installmentCount)
        {
            var a = new DueDateAdjuster();
            var ddc = new PaymentDueDateCalculator(a);

            var result = ddc.Execute(DateTime.Now, 14, installmentCount);
        }

        [Theory]
        [InlineData(14)]
        [InlineData(17)]
        [InlineData(21)]
        public void Interval_between_dates_should_match_supplied_interval(int installmentInterval)
        {
            var a = new DueDateAdjuster();
            var ddc = new PaymentDueDateCalculator(a);

            var dueDate = DateTime.Now;
            
            var result = ddc.Execute(dueDate, installmentInterval, 4);

            foreach (var item in result)
            {
                item.Date.ShouldBe(dueDate.Date);

                dueDate = dueDate.AddDays(installmentInterval);
            }
        }

        [Fact]
        public void First_due_date_should_equal_to_start_date()
        {
            var a =  new DueDateAdjuster();
            var ddc = new PaymentDueDateCalculator(a);

            var dueDate = DateTime.Now;

            var result = ddc.Execute(dueDate, 14, 4);

            result.First().Date.ShouldBe(dueDate.Date);
        }
    }
}
