﻿using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Text;
using QuadPay.Domain;
using QuadPay.Domain.Infrastructure;
using Shouldly;
using Xunit;

namespace QuadPay.Test.UnitTests
{
    public class Test_Installment
    {
        public class Ctor
        {

            [Fact]
            public void When_valid_args_provided_can_construct()
            {
                var amount = 100;
                var dueDate = DateTime.Now.AddDays(10);

                var sut = new Installment(amount, dueDate);

                sut.Amount.Value.ShouldBe(amount, "Amount");

                sut.DueDate.ShouldBe(dueDate, "DueDate");
            }

            [Fact]
            public void When_constructed_sets_status_to_Pending()
            {
                var amount = 100;
                var dueDate = DateTime.Now.AddDays(10);

                var sut = new Installment(amount, dueDate);

                sut.Status.ShouldBe(InstallmentStatus.Pending);
            }

            [Fact]
            public void When_constructed_sets_Id()
            {
                var amount = 100;
                var dueDate = DateTime.Now.AddDays(10);

                var sut = new Installment(amount, dueDate);

                sut.Id.ShouldNotBe(Guid.Empty);
            }

        }

        public class SetPaid
        {

            [Fact]
            public void When_called_with_valid_paymentReference_changes_state()
            {
                var amount = 100;
                var dueDate = DateTime.Now;

                var sut = new Installment(amount, dueDate);

                var paymentRef = Guid.NewGuid().ToString();

                sut.SetPaid(paymentRef);

                sut.SettlementDate.Date.ShouldBe(DateTime.Now.Date, "SettlementDate");

                sut.PaymentReference.ShouldBe(paymentRef, "PaymentReference");

                sut.Status.ShouldBe(InstallmentStatus.Paid);

                sut.AvailableForRefund().ShouldBe(sut.Amount.Value);

            }

            [Fact]
            public void When_paymentReference_is_null_throws()
            {
                var amount = 100;
                var dueDate = DateTime.Now;

                var sut = new Installment(amount, dueDate);

                var ex = Assert.Throws<ArgumentNullException>(() => sut.SetPaid(null));

                //NOTE: we should also check that state of the object has not changed before exception was thrown.
            }

            [Fact]
            public void When_paid_cannot_call_again()
            {
                var amount = 100;
                var dueDate = DateTime.Now;

                var sut = new Installment(amount, dueDate);

                sut.SetPaid(Guid.NewGuid().ToString());

                var ex = Assert.Throws<ValidationException>(() => sut.SetPaid(Guid.NewGuid().ToString()));

                ex.Message.ShouldBe("Installment has already been paid.");

                //NOTE: we should also check that state of the object has not changed before exception was thrown.

            }
        }

        public class Refund
        {

            [Fact]
            public void When_called_with_null_throws()
            {
                var amount = 100;
                var dueDate = DateTime.Now;

                var sut = new Installment(amount, dueDate);

               Assert.Throws<ArgumentNullException>(()=> sut.Refund(null));
            }

            [Fact]
            public void When_trying_to_refund_installment_that_has_not_been_paid_throws()
            {
                var amount = 100;
                var dueDate = DateTime.Now;

                var sut = new Installment(amount, dueDate);
                var ex =Assert.Throws<ValidationException>(() => sut.Refund(100));

                ex.Message.ShouldBe("Cannot refund installment that has not been paid.");
            }

            [Fact]
            public void When_trying_to_refund_more_than_available_for_refund_throws()
            {
                var amount = 100;
                var dueDate = DateTime.Now;

                var sut = new Installment(amount, dueDate);

                sut.SetPaid(Guid.NewGuid().ToString());

                var ex = Assert.Throws<ValidationException>(() => sut.Refund(200));

                ex.Message.ShouldBe("Cannot refund more that available for refund.");
            }

            [Fact]
            public void Can_refund_less_than_available_amount()
            {
                var amount = 100;
                var dueDate = DateTime.Now;

                var sut = new Installment(amount, dueDate);

                sut.SetPaid(Guid.NewGuid().ToString());

                sut.Refund(10);

                sut.Amount.Value.ShouldBe(100);
                sut.AvailableForRefund().ShouldBe(90);
                sut.IsPaid().ShouldBe(true);
                sut.Status.ShouldBe(InstallmentStatus.Paid);
            }

            [Fact]
            public void Can_refund_complete_installment_amount()
            {
                var amount = 100;
                var dueDate = DateTime.Now;

                var sut = new Installment(amount, dueDate);

                sut.SetPaid(Guid.NewGuid().ToString());

                sut.Refund(100);

                sut.Amount.Value.ShouldBe(100);
                sut.AvailableForRefund().ShouldBe(0);
                sut.IsPaid().ShouldBe(true);
                sut.Status.ShouldBe(InstallmentStatus.Paid);
            }
        }
    }
}

