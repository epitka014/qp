﻿using System;
using System.Collections.Generic;
using System.Text;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.ValueObjects;
using Shouldly;
using Xunit;

namespace QuadPay.Test.UnitTests.ValueObjects
{
    public class Test_InstallmentInterval
    {

        public class Ctor
        {
            [Fact]
            public void Will_construct_with_default_interval_when_one_not_provided()
            {
                var sut = new InstallmentInterval();

                sut.Value.ShouldBe(14);

            }

            [Fact]
            void Will_construct_when_valid_argument_provided()
            {
                var sut = new InstallmentInterval(15);
                sut.Value.ShouldBe(15);
            }

            [Theory]
            [InlineData(-1)]
            [InlineData(13)]
            [InlineData(22)]
            void When_outside_of_range_of_valid_values_will_throw(int value)
            {
                var ex = Assert.Throws<ValidationException>(() => new InstallmentInterval(value));

                ex.Message.ShouldBe("Installment interval cannot be less than 14 day and more than 21 days");
            }
        }

        public class ImplicitOperator
        {
            [Fact]
            public void Can_Convert_Int_To_InstallmentCount()
            {
                var expected = 4;

                var sut = new InstallmentCount(expected);

                Assert.True(sut == expected);
            }
        }
    }
}
