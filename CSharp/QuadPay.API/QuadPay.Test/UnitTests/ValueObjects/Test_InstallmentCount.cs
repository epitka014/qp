﻿using System;
using System.Collections.Generic;
using System.Text;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.ValueObjects;
using Shouldly;
using Xunit;

namespace QuadPay.Test.UnitTests.ValueObjects
{
    public class Test_IntallmentCount
    {
        public class Ctor
        {
            [Fact]
            public void Will_construct_with_default_count_when_one_not_provided()
            {
                var sut = new InstallmentCount();

                sut.Value.ShouldBe(4);

            }

            [Fact]
            void Will_construct_when_valid_argument_provided()
            {
                var sut = new InstallmentCount(5);
                sut.Value.ShouldBe(5);
            }

            [Theory]
            [InlineData(-1)]
            [InlineData(3)]
            [InlineData(9)]
            void When_outside_of_range_of_valid_values_will_throw(int value)
            {
                var ex = Assert.Throws<ValidationException>(() => new InstallmentCount(value));

                ex.Message.ShouldBe("Number of installments cannot be less than 4 and more than 8");
            }
        }

        public class ImplicitOperator
        {
            [Fact]
            public void Can_Convert_Int_To_InstallmentCount()
            {
                var expected = 4;

                var sut = new InstallmentCount(expected);

                Assert.True(sut == expected);
            }

            
        }

    }
}

