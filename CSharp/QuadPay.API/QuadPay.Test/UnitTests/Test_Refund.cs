﻿using System;
using System.Collections.Generic;
using System.Text;
using QuadPay.Domain;
using QuadPay.Domain.Infrastructure;
using QuadPay.Domain.ValueObjects;
using Shouldly;
using Xunit;

namespace QuadPay.Test
{
   public class Test_Refund
    {
        public class Ctor
        {
            [Fact]
            public void When_called_with_null_IdempotencyKey_throws()
            {
                Assert.Throws<ArgumentNullException>(() => new Refund(null, 100));
            }

            [Fact]
            public void When_called_with_null_Money_throws()
            {
                Assert.Throws<ArgumentNullException>(() => new Refund(Guid.NewGuid().ToString(), null));
            }

            [Fact]
            public void When_amount_is_0_throws()
            {
                var ex = Assert.Throws<ValidationException>(() => new Refund(Guid.NewGuid().ToString(), 0));
                ex.Message.ShouldBe("Amount to refund must be greater than 0");
            }

            [Fact]
            public void Can_construct_when_called_with_valid_args()
            {
                var key = new IdempotencyKey(Guid.NewGuid().ToString());

                var sut = new Refund(key,100);

                sut.Amount.Value.ShouldBe(100,"Amount");
                sut.Date.Date.ShouldBe(DateTime.Now.Date,"Date");
                sut.IdempotencyKey.ShouldBe(key,"IdempotencyKey");
                sut.Id.ShouldNotBeNull("Id");

            }
        }
    }
}
